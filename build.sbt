import sbtassembly.AssemblyPlugin._

name := "acute"
scalaVersion := "2.11.7"
sbtVersion := "0.13.11"
organization := "org.janzhou"

enablePlugins(GitVersioning)

scalacOptions ++= Seq("-Yinline-warnings", "-optimise", "-feature", "-deprecation")

lazy val root = (project in file(".")).
enablePlugins(PlayScala).
enablePlugins(BuildInfoPlugin).
settings(
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
  buildInfoPackage := "acute"
)

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-slick" % "2.0.2",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.2",
  "com.h2database" % "h2" % "1.3.176",
  cache,
  ws,
 "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
)

routesGenerator := InjectedRoutesGenerator
unmanagedResourceDirectories in Compile += { baseDirectory.value / "public" }

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) => {
    case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.last
    case x => old(x)
  }
}

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
