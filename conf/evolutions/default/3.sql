# --- !Ups

ALTER TABLE "orders" MODIFY COLUMN "ship" int

# --- !Downs

ALTER TABLE "orders" MODIFY COLUMN "ship" varchar(32)
