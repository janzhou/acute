# --- !Ups

ALTER TABLE "orders" MODIFY COLUMN "status" int

# --- !Downs

ALTER TABLE "orders" MODIFY COLUMN "status" bigint
