/*
// ==UserScript==
// @name        stoexpress
// @namespace   kaixindeal.com
// @include     https://*.stoexpress.us/*
// @version     1
// @grant       none
// ==/UserScript==

var kaixindeal = document.createElement("script");
kaixindeal.setAttribute("src",
"https://kaixindeal.com/assets/javascripts/stoexpress.js"
);
kaixindeal.setAttribute("charset", "utf-8")
document.body.appendChild(kaixindeal);
*/

function stoSetup(){
  var inputs = document.querySelectorAll('input[checked="checked"]')

  for(var input of inputs ){
    input.checked = false;
  }

  document.getElementById("5060W.ColonialDr.Suite101A").checked = true;
  document.getElementById("agree").checked = true;

  $('html, body').animate({
    scrollTop: $("#NewReceiveAddress").offset().top
  }, 0);

  $('#NewReceiveAddress').trigger('click');

  $('#txtRece_name').change(function(){
    stoSetContact($('#txtRece_name').val());
  }).focus();
}

function stoSetContact(input){
  var contact = JSON.parse(input)

  $('#txtRece_name').val(contact.name);
  $('#txtRece_mobilephone').val(contact.phone);
  $('#txtRece_province').val(contact.state);
  $('#txtRece_city').val(contact.city);
  $('#txtqu').val(contact.area);
  $('#txtRece_address').val(contact.street);
  $('#txtRece_postoffice').val(contact.zip);

  if (typeof contact.memo !== "undefined") {
    $('#Memo').val(contact.memo);
  }
};

if( window.location.href.includes(
  "https://account.stoexpress.us/member/stoYundanAdd_KD.aspx?m=16"
)) {
  stoSetup()
}
