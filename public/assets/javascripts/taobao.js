/*
// ==UserScript==
// @name        taobao
// @namespace   kaixindeal.com
// @include     https://*.taobao.com/*
// @version     1
// @grant       none
// ==/UserScript==

var kaixindeal = document.createElement("script");
kaixindeal.setAttribute("src",
"https://kaixindeal.com/assets/javascripts/taobao.js"
);
kaixindeal.setAttribute("charset", "utf-8")
document.body.appendChild(kaixindeal);
*/
function addZeroClipboard() {
  var script = document.createElement("script");
  script.src = "//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.2.0/ZeroClipboard.js";
  script.type = "text/javascript";
  document.body.appendChild(script);
}

function addJQuery(callback) {
  var script = document.createElement("script");
  script.setAttribute("src", "//code.jquery.com/jquery-2.2.3.min.js");
  script.addEventListener('load', function() {
    var script = document.createElement("script");
    script.textContent = "window.jQ=jQuery.noConflict(true);(" + callback.toString() + ")();";
    document.body.appendChild(script);
  }, false);
  document.body.appendChild(script);
}

function main() {
  function updateUI(){
    if( jQ("#J_BlockBuyerAddress").text().length > 11 ) {
      jQ("#ship-info").remove();
      jQ("#sto-span").remove();
      jQ("div.trade-status").append('<p id="ship-info">' + jQ("#J_BlockBuyerAddress").text() + '，' + jQ("span.order-num").text() + '</p>');
      jQ(".actions").append('<span class="skin-gray" id="sto-span"><button class="short-btn" id="sto-button" title="申通填单">申通</button></span>');
      var client = new ZeroClipboard( document.getElementById("sto-button") );
      client.on( "copy", function (event) {
        var contact = jQ("#ship-info").text().split("，")
        var address = contact[2].split(" ")
        event.clipboardData.setData( "text/plain", JSON.stringify(
          {
            name: contact[0],
            phone: contact[1],
            state: address[0],
            city: address[1],
            area: address[2],
            zip: contact[3],
            street:contact[2],
            memo: "https://trade.taobao.com/trade/detail/trade_item_detail.htm?&bizOrderId=" + contact[4]

          }
        ) );
        window.open("https://account.stoexpress.us/member/stoYundanAdd_KD.aspx?m=16", '_blank');
      });
    } else {
      window.setTimeout( updateUI, 1000 );
    }
  }
  updateUI();
}

function setTaobao() {
  console.log("kaixindeal in taobao")
  addZeroClipboard();
  addJQuery(main);
}

if( window.location.href.includes(
  "https://trade.taobao.com/trade/detail/trade_item_detail.htm"
)) {
  setTaobao()
}
