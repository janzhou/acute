function ExtractId(str){
    var params = str.split('-');
    return params[params.length - 1];
}

function form_setup(){
  $("form").submit(function(e) {

    var ref = $(this).find("[required]");

    $(ref).each(function(){
      if ( $(this).val() == '' )
      {
        alert("Required field should not be blank.");

        $(this).focus();

        e.preventDefault();
        return false;
      }
    });  return true;
  });
}

function admin_order_list() {
  $(".order-status").change(function(e){
    var id = ExtractId(e.target.id)
    var status = e.target.value
    $.ajax({
      url: "/api/orderUpdateStatus?id=" + id + "&status=" + status,
      success: function(result) {
        $("#status-" + result.id).value = result.status
      }
    });
  });
  $(".order-track").change(function(e){
    var id = ExtractId(e.target.id)
    var track = $("#track-" + id ).val()
    var ship = $("#ship-" + id ).val()
    if( track.length > 0 && ship >= 0 ) {
      $.ajax({
        url: "/api/orderUpdateShipping?id=" + id + "&ship=" + ship + "&track=" + track,
        success: function(result) {
        }
      })
    }
  });
}

$(document).ready(function() {
  if( window.location.href.includes(
    "admin/order/"
  )) {
    admin_order_list()
  }

  form_setup()
});
