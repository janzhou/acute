package controllers

import scala.concurrent.Future

import play.api.libs.json.Json
import play.api._
import play.api.mvc._
import play.api.cache._
import play.api.Play.current

import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.text

import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import java.sql.Timestamp
import java.util.Calendar

import models.User
import models.Order
import models.Constant._
import dao.UserDAO
import dao.OrderDAO

class Admin@Inject() (userDao: UserDAO, orderDao: OrderDAO)
extends Controller {

  def userAll = Action.async { implicit request =>
    request.session.get("connected").map { user =>
      userDao.getPermission(user.toLong) match {
        case p if Constant.isAdmin(p) => {
          userDao.all().map(users => Ok(views.html.user_list("用户列表", "admin_user_list", Constant)(users)))
        }
        case p if Constant.isUser(p) => {
          Future {} .map( _ => Ok(views.html.print("没有权限", "order_list", Constant)("没有权限")) )
        }
      }
    }.getOrElse {
      Future {} .map( _ => Ok(views.html.user_form("用户登录", "login", Constant)("", "/user/login", "登录")) )
    }
  }

  def status1stPage(status:String) = {
    statusPage(status, 0)
  }

  def statusPage(status:String, page:Long) = {
    val s = Constant.status.filter(_._3 == status)
    val ss = if ( s.length == 0 ) {
      -1
    } else {
      s.head._1
    }

    if( ss == -1 ) {
      orderAll(page)
    } else {
      orderStatus(page, ss)
    }
  }

  def orderAll(page:Long) = Action.async { implicit request =>
    request.session.get("connected").map { connected =>
      val user = connected.toLong
      userDao.getPermission(user) match {
        case p if Constant.isManager(p) => {
          orderDao.all(
            page * Constant.orders_per_page, Constant.orders_per_page
          )
          .map(orders =>
            Ok(views.html.admin_order_list("订单列表", "admin_order_list", Constant)(orders, page, -1))
          )
        }
        case p if Constant.isAssist(p) => {
          orderDao.findByAssist(user,
            page * Constant.orders_per_page, Constant.orders_per_page
          )
          .map(orders =>
            Ok(views.html.admin_order_list("订单列表", "admin_order_list", Constant)(orders, page, -1))
          )
        }
        case p if Constant.isUser(p) => {
          scala.concurrent.Future {}
          .map( _ => Redirect(routes.Order.status1stPage("all")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ => Redirect(routes.User.loginGet) )
    }
  }

  private def orderStatus(page:Long, status:Int) = Action.async { implicit request =>
    request.session.get("connected").map { connected =>
      val user = connected.toLong
      userDao.getPermission(user) match {
        case p if Constant.isManager(p) => {
          orderDao.all(
            page * Constant.orders_per_page, Constant.orders_per_page
            , status
          )
          .map(orders =>
            Ok(views.html.admin_order_list("订单列表", "admin_order_list", Constant)(orders, page, status))
          )
        }
        case p if Constant.isAssist(p) => {
          orderDao.findByAssist(user,
            page * Constant.orders_per_page, Constant.orders_per_page
            , status
          )
          .map(orders =>
            Ok(views.html.admin_order_list("订单列表", "admin_order_list", Constant)(orders, page, status))
          )
        }
        case p if Constant.isUser(p) => {
          scala.concurrent.Future {}
          .map( _ => Redirect(routes.Order.status1stPage("all")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ => Redirect(routes.User.loginGet) )
    }
  }

  def orderGet(id:Long) = Action.async { implicit request =>
    request.session.get("connected").map { connected =>
      val user = connected.toLong
      userDao.getPermission(user) match {
        case p if Constant.isManager(p) => {
          orderDao.findById(id).map(order => order match {
            case Some(o) =>
            Ok(views.html.admin_order("订单详情", "admin_order_list", Constant)(o, userDao.findByIdWait(o.user)))
            case None => NotFound
          })
        }
        case p if Constant.isAssist(p) => {
          orderDao.findById(id).map(order => order match {
            case Some(o) => if (o.user == user || o.assist == user) {
              Ok(views.html.admin_order("订单详情", "admin_order_list", Constant)(o, userDao.findByIdWait(o.user)))
            } else NotFound
            case None => NotFound
          })
        }
        case p if Constant.isUser(p) => {
          scala.concurrent.Future {}
          .map( _ => Redirect(routes.Order.id(id)) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ => Redirect(routes.User.loginGet) )
    }
  }
}
