package controllers

import models.Constant._

import play.api._
import play.api.mvc._
import play.api.cache._
import play.api.Play.current

import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.text

import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import java.sql.Timestamp
import java.util.Calendar

import dao.UserDAO
import dao.OrderDAO


class Order@Inject() (userDao: UserDAO, orderDao: OrderDAO)
extends Controller {

  def status1stPage(status:String) = {
    statusPage(status, 0)
  }

  def statusPage(status:String, page:Long) = {
    val s = Constant.status.filter(_._3 == status)
    val ss = if ( s.length == 0 ) {
      -1
    } else {
      s.head._1
    }

    if( ss == -1 ) {
      all(page)
    } else {
      orderStatus(page, ss)
    }
  }

  def allFirstPage = all(0)
  def all(page: Long) = Action.async { implicit request =>
    request.session.get("connected").map { connected =>
      val user = connected.toLong
      userDao.getPermission(user) match {
        case p if Constant.isUser(p) => {
          orderDao.findByUser(user,
            page * Constant.orders_per_page, Constant.orders_per_page
          )
          .map(orders =>
            Ok(views.html.order_list("订单列表", "order_list", Constant)(orders, page, -1))
          )
        }
        case p if Constant.isAdmin(p) => {
          scala.concurrent.Future {}
          .map( _ => Redirect(routes.Admin.status1stPage("all")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ => Redirect(routes.User.loginGet) )
    }
  }

  private def orderStatus(page: Long, status:Int) = Action.async { implicit request =>
    request.session.get("connected").map { connected =>
      val user = connected.toLong
      userDao.getPermission(user) match {
        case p if Constant.isUser(p) => {
          orderDao.findByUser(user,
            page * Constant.orders_per_page, Constant.orders_per_page
            ,status
          )
          .map(orders =>
            Ok(views.html.order_list("订单列表", "order_list", Constant)(orders, page, status))
          )
        }
        case p if Constant.isAdmin(p) => {
          scala.concurrent.Future {}
          .map( _ => Redirect(routes.Admin.status1stPage("all")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ => Redirect(routes.User.loginGet) )
    }
  }

  def id(id:Long) = Action.async { implicit request =>
    request.session.get("connected").map { connected =>
      userDao.getPermission(connected.toLong) match {
        case p if Constant.isUser(p) => {
          orderDao.findById(id).map(order => order match {
            case Some(o) => if ( o.user == connected.toLong ) {
              Ok(views.html.order_show("订单列表", "order_list", Constant)(o, userDao.findByIdWait(o.user)))
            } else {
              Redirect(routes.Order.status1stPage("all"))
            }
            case None => NotFound
          })
        }
        case p if Constant.isAdmin(p) => {
          scala.concurrent.Future {}
          .map( _ => Redirect(routes.Admin.orderGet(id)) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ => Redirect(routes.User.loginGet) )
    }
  }

  def createGet = Action { implicit request =>
    request.session.get("connected").map { connected =>
      Ok(views.html.order_create("创建订单", "order_create", Constant))
    }.getOrElse {
      Ok(views.html.order_create_guest("创建订单", "order_create_guest", Constant))
    }
  }

  case class guestOrderCase(
    email:String,
    password:String,
    name:String,
    state:String,
    city:String,
    area:String,
    street:String,
    zip:String,
    phone:String,
    memo:String
  )

  val guestOrderForm = Form(
    mapping(
      "email" -> text(),
      "password" -> text(),
      "name" -> text(),
      "state" -> text(),
      "city" -> text(),
      "area" -> text(),
      "street" -> text(),
      "zip" -> text(),
      "phone" -> text(),
      "memo" -> text()
    )(guestOrderCase.apply)(guestOrderCase.unapply)
  )

  def guestPost = Action.async(parse.form(guestOrderForm)) { implicit request =>
    val newOrder = request.body

    var newUser = false
    val user_id = if( newOrder.email.length == 0 ){
      1
    } else { (userDao.findByEmailWait(newOrder.email) match {
      case Some(u:models.User) => {
        userDao.findByEmailPasswordWait(newOrder.email, userDao.hash(newOrder.password)) match {
          case Some(u:models.User) => u
          case None => new models.User(None, "", Array[Byte](), 0L)
        }
      }
      case None => {
        newUser = true
        userDao.insertWait(
          new models.User(None, newOrder.email,
            userDao.hash(newOrder.email), Constant.permit("user")
          )
        )
      }
    }).id match {
      case Some(id:Long) => id
      case None => -1
    }
  }

  if( user_id < 0 ) {
    scala.concurrent.Future {}
    .map( _ => Redirect(routes.Order.createGet) )
  } else {
    val timestamp = getTimestamp
    val order = new models.Order(None, user_id, 0,
      newOrder.name,
      newOrder.state,
      newOrder.city,
      newOrder.area,
      newOrder.street,
      newOrder.zip,
      newOrder.phone, 1L, None, None, timestamp, timestamp, Some(newOrder.memo)
    )
    orderDao.insert(order).map( order => order.id match {
      case Some(id:Long) => {
        if( newOrder.email.length == 0 ) {
          Ok(views.html.print("订单创建成功", "order_create_guest", Constant)("订单号：" + id))
        } else {
          Redirect(routes.Order.id(id))
          .withSession(("connected", user_id.toString))
        }
      }
      case None => NotFound
    })
  }
}

case class orderCase(
  name:String,
  state:String,
  city:String,
  area:String,
  street:String,
  zip:String,
  phone:String,
  memo:String
)

private val orderForm = Form(
  mapping(
    "name" -> text(),
    "state" -> text(),
    "city" -> text(),
    "area" -> text(),
    "street" -> text(),
    "zip" -> text(),
    "phone" -> text(),
    "memo" -> text()
  )(orderCase.apply)(orderCase.unapply)
)

def createPost = Action.async(parse.form(orderForm)) { implicit request =>
  request.session.get("connected").map { connected =>
    val newOrder = request.body
    val timestamp = getTimestamp
    val order = new models.Order(None, connected.toLong, 0,
      newOrder.name,
      newOrder.state,
      newOrder.city,
      newOrder.area,
      newOrder.street,
      newOrder.zip,
      newOrder.phone, 1L, None, None, timestamp, timestamp, Some(newOrder.memo))
      orderDao.insert(order).map( order => order.id match {
        case Some(id:Long) => Redirect(routes.Order.id(id))
        case None => NotFound
      })
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ => Redirect(routes.User.loginGet) )
    }
  }

  private def getTimestamp = {
    new Timestamp(Calendar.getInstance().getTime().getTime())
  }
}
