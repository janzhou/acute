package controllers

import scala.concurrent.Future

import play.api._
import play.api.mvc._
import play.api.cache._
import play.api.Play.current

import javax.inject.Inject
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.text
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.Action
import play.api.mvc.Controller

import dao.UserDAO
import models.Constant._

class User@Inject() (userDao: UserDAO)
extends Controller {
  def logoutGet = Action { implicit request =>
    Redirect(routes.User.loginGet).withNewSession
  }

  def loginGet = Action { implicit request =>
    request.session.get("connected").map { user =>
      Redirect(routes.Order.status1stPage("all"))
    }.getOrElse {
      Ok(views.html.user_form("用户登录", "login", Constant)("", "/user/login", "登录"))
    }
  }

  case class userCase(email:String, password:String)

  private val userForm = Form(
    mapping(
      "email" -> text(),
      "password" -> text()
    )(userCase.apply)(userCase.unapply)
  )

  def loginPost =
     Action.async(parse.form(userForm)) { implicit request =>
       val userLogin = request.body
       userDao.findByEmailPassword(userLogin.email, userDao.hash(userLogin.password))
       .map(user => user match {
         case Some(u:models.User) => u.id match {
           case Some(id:Long) =>
           Redirect(routes.Order.status1stPage("all"))
           .withSession(("connected", id.toString))
           case None =>
           Ok(views.html.user_form("用户登录", "login", Constant)("用户不存在或密码错误", "/user/login", "登录"))
         }
         case None =>
           Ok(views.html.user_form("用户登录", "login", Constant)("用户不存在或密码错误", "/user/login", "登录"))
       })
     }

  def createGet = Action { implicit request =>
    request.session.get("connected").map { user =>
      Redirect(routes.Order.status1stPage("all"))
    }.getOrElse {
      Ok(views.html.user_form("创建用户", "user_create", Constant)("用户不存在或密码错误", "/user/create", "创建"))
    }
  }

  def createPost =
     Action.async(parse.form(userForm)) { implicit request =>
       val newUser = request.body
       val user = new models.User(None, newUser.email, userDao.hash(newUser.password), Constant.permit("user"))
       userDao.insert(user).map(user => user.id match {
         case Some(id:Long) =>
         if( id == 1 ) {
           val user = new models.User(Some(id), newUser.email, userDao.hash(newUser.password), Constant.permit("root"))
           userDao.update(id, user)
         }
         Redirect(routes.Order.status1stPage("all"))
         .withSession(("connected", id.toString))
         case None =>
         Ok(views.html.user_form("创建用户", "user_create", Constant)("用户创建失败", "/user/create", "创建"))
       })
     }
}
