package controllers

import play.api._
import play.api.mvc._
import play.api.cache._
import play.api.Play.current

import javax.inject.Inject
import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.text
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.Action
import play.api.mvc.Controller

class Application @Inject()
extends Controller {
  def index = Action { implicit request =>
    request.session.get("connected").map { connected =>
      Redirect(routes.Order.status1stPage("all"))
    }.getOrElse {
      Redirect(routes.User.loginGet)
    }
  }
}
