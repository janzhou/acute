package dao

import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

import javax.inject.Inject
import models.User
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.db.NamedDatabase
import slick.driver.JdbcProfile

import javax.crypto.spec.SecretKeySpec
import javax.crypto.Mac

class UserDAO @Inject()(@NamedDatabase("default") protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  private val Users = TableQuery[UserTable]

  def all(): Future[Seq[User]] = db.run(Users.result)

  def insert(user: User): Future[User] = {
    val insertQuery = Users returning Users.map(_.id) into ((user, id) => user.copy(id = Some(id)))
    val action = insertQuery += user
    db.run(action)
  }

  def insertWait(user: User): User = {
    val query = insert(user)
    Await.result(query, 1 seconds)
  }

  def update(id:Long, user:User):Future[Int] =
    db.run(Users.filter(_.id === id).update(user))

  def getPermission(id:Long):Long = {
    val user = findByIdWait(id)
    if(user == null) {
      -1L
    } else user.permission
  }

  def findById(id: Long):Future[Option[User]] =
    db.run(Users.filter(_.id === id).result.headOption)

  def findByIdWait(id: Long):User = {
    val query = findById(id).map( user => user match {
      case Some(u) => u
      case None => null
    })
    Await.result(query, 1 seconds)
  }

  def findByEmail(email: String):Future[Option[User]] =
    db.run(Users.filter(_.email === email).result.headOption)

  def findByEmailWait(email: String):Option[User] = {
    val query = findByEmail(email)
    Await.result(query, 1 seconds)
  }

  def findByEmailPassword(email: String, password:Array[Byte])
  :Future[Option[User]] =
    db.run(Users.filter(u => u.email === email && u.password === password).result.headOption)

  def findByEmailPasswordWait(email: String, password:Array[Byte]) = {
    val query = findByEmailPassword(email, password)
    Await.result(query, 1 seconds)
  }

  //def update(id:Long, user:User):Future[Long] =
    //db.run(Users.filter(_.id === id).update(user))

  def updateWait(id:Long, user:User):Int = {
    val query = update(id, user)
    Await.result(query, 1 seconds)
  }

  private class UserTable(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def email = column[String]("email", O.PrimaryKey)
    def password = column[Array[Byte]]("password")
    def permission = column[Long]("permission")

    def * = (id.?, email, password, permission) <> (User.tupled, User.unapply _)
  }

  private val sha1 = java.security.MessageDigest.getInstance("SHA-1")
  private val secret = new SecretKeySpec("QcC+iE52sT6BPQsdfjmmj37ct7tHy2aPyxKoSY5Rv".getBytes, "HmacSHA1")
  private val hmac = Mac.getInstance("HmacSHA1")
  hmac.init(secret)

  def hash(s:String):Array[Byte] = {
    hmac.doFinal(s.getBytes)
  }
}
