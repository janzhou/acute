package dao

import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

import javax.inject.Inject
import models.Order
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.db.NamedDatabase
import slick.driver.JdbcProfile
import java.sql.Timestamp
import java.util.Calendar

class OrderDAO @Inject()(@NamedDatabase("default") protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  private val Orders = TableQuery[OrderTable]

  private def getTimestamp = {
    new Timestamp(Calendar.getInstance().getTime().getTime())
  }

  def all(drop:Long, take:Long): Future[Seq[Order]] = db.run(
    Orders.sortBy(_.created.desc).drop(drop).take(take).result
  )

  def findByUser(user:Long, drop:Long, take:Long): Future[Seq[Order]] = db.run(
    Orders.filter(_.user === user)
    .sortBy(_.created.desc).drop(drop).take(take).result
  )

  def findByAssist(assist:Long, drop:Long, take:Long): Future[Seq[Order]] = db.run(
    Orders.filter(_.assist === assist)
    .sortBy(_.created.desc).drop(drop).take(take).result
  )

  def all(drop:Long, take:Long, status:Int): Future[Seq[Order]] = db.run(
    Orders.filter(_.status === status)
    .sortBy(_.created.desc).drop(drop).take(take).result
  )

  def findByUser(user:Long, drop:Long, take:Long, status:Int)
  : Future[Seq[Order]] = db.run(
    Orders.filter(_.user === user).filter(_.status === status)
    .sortBy(_.created.desc).drop(drop).take(take).result
  )

  def findByAssist(assist:Long, drop:Long, take:Long, status:Int)
  : Future[Seq[Order]] = db.run(
    Orders.filter(_.assist === assist).filter(_.status === status)
    .sortBy(_.created.desc).drop(drop).take(take).result
  )

  def findById(id: Long):Future[Option[Order]] = db.run(Orders.filter(_.id === id).result.headOption)

  def findByIdWait(id: Long):Order = {
    val query = findById(id).map( order => order match {
      case Some(o) => o
      case None => null
    })
    Await.result(query, 1 seconds)
  }

  def insert(order: Order):Future[Order] = {
    val insertQuery = Orders returning Orders.map(_.id) into ((order, id) => order.copy(id = Some(id)))
    val action = insertQuery += order
    db.run(action)
  }

  def update(id:Long, order:Order):Future[Int] =
    db.run(Orders.filter(_.id === id).update(order.copy(updated = getTimestamp)))

  def updateWait(id:Long, order:Order):Int = {
    val query = update(id, order)
    Await.result(query, 1 seconds)
  }

  private class OrderTable(tag: Tag) extends Table[Order](tag, "orders") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def user = column[Long]("user")
    def status = column[Int]("status")
    def name = column[String]("name")
    def state = column[String]("state")
    def city = column[String]("city")
    def area = column[String]("area")
    def street = column[String]("street")
    def zip = column[String]("zip")
    def phone = column[String]("phone")
    def assist = column[Long]("assist")
    def ship = column[Int]("ship")
    def track = column[String]("track")

    def created = column[Timestamp]("created")
    def updated = column[Timestamp]("updated")

    def memo = column[String]("memo")

    def * = (id.?, user, status, name, state, city, area, street, zip, phone, assist, ship.?, track.?, created, updated, memo.?) <> (Order.tupled, Order.unapply _)
  }
}
