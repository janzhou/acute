package models
import java.sql.Timestamp

case class User(
  id:Option[Long],
  email: String,
  password: Array[Byte],
  permission:Long
)
case class Order(
  id:Option[Long],
  user: Long,
  status: Int,
  name:String,
  state:String,
  city:String,
  area:String,
  street:String,
  zip:String,
  phone:String,
  assist:Long,
  ship:Option[Int],
  track:Option[String],
  created:Timestamp,
  updated:Timestamp,
  memo:Option[String]
)
