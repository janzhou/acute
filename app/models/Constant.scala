package models

class Constant {
  private val pack = this.getClass.getPackage
  // val app = pack.getImplementationTitle
  // val version = pack.getImplementationVersion
  val app = acute.BuildInfo.name
  val version = acute.BuildInfo.version
  val name = "开心北美购物"
  val host = "kaixindeal.com"
  val status = Array(
    (-1, "全部订单", "all"),
    (0,  "未确认",   "unconfirmed"),
    (1,  "已确认",   "confirmed"),
    (2,  "已发货",   "shipped"),
    (4,  "已签收",   "finish"),
    (3,  "已关闭",   "closed")
  )

  val ship = Map(
    -1 -> ("未设置",     {x:String => "http://kaixindeal.com/"}),
    0  -> ("申通*普货",   {x:String => "http://www.stoexpress.us/"}),
    1  -> ("申通*包包",   {x:String => "http://www.stoexpress.us/"}),
    2  -> ("阳光国际",    {x:String => "http://www.shiningexpress.com/route-kd100.jsp?oid=" + x})
  )

  val permit = Map(
    "user" -> 0x0000L,
    "assist" -> 0x00FFL,
    "manager" -> 0xFFFFL,
    "root" -> Long.MaxValue
  )

  def isGuest(p: Long):Boolean = {
    p < permit("user")
  }

  def isUser(p: Long):Boolean = {
    p >= permit("user") && p < permit("assist")
  }

  def isAssist(p: Long):Boolean = {
    p >= permit("assist") && p < permit("manager")
  }

  def isManager(p: Long):Boolean = {
    p >= permit("manager")
  }

  def isAdmin(p: Long):Boolean = {
    p >= permit("assist")
  }

  def isRoot(p: Long):Boolean = {
    p == permit("root")
  }

  val orders_per_page = 50
}

object Constant {
  val Constant = new Constant
}
