package api

import scala.concurrent.Future

import play.api.libs.json.Json
import play.api._
import play.api.mvc._
import play.api.cache._
import play.api.Play.current

import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.text

import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import java.sql.Timestamp
import java.util.Calendar

import models.Constant._
import dao.UserDAO
import dao.OrderDAO

class User@Inject() (userDao: UserDAO, orderDao: OrderDAO)
extends Controller {
  def UpdatePermission(id:Long, permission:Long) = Action.async {
    implicit request =>
    request.session.get("connected").map { connected =>
      userDao.getPermission(connected.toLong) match {
        case p if Constant.isRoot(p) => {
          userDao.findById(id).map(user => user match {
            case Some(u) => {
              val user = u.copy(permission = permission)
              userDao.updateWait(id, user)
              val newUser = userDao.findByIdWait(id)
              Ok(Json.obj("id" -> id, "permission" -> newUser.permission))
            }
            case None => Ok(Json.obj("error" -> "NotFound"))
          })
        }
        case _ => {
          scala.concurrent.Future {}
          .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
    }
  }
}
