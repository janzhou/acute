package api

import scala.concurrent.Future

import play.api.libs.json.Json
import play.api._
import play.api.mvc._
import play.api.cache._
import play.api.Play.current

import play.api.data.Form
import play.api.data.Forms.mapping
import play.api.data.Forms.text

import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import java.sql.Timestamp
import java.util.Calendar

import models.Constant._
import dao.UserDAO
import dao.OrderDAO

class Order@Inject() (userDao: UserDAO, orderDao: OrderDAO)
extends Controller {
  def UpdateAssist(
    id:Long,
    newAssist:Long
  ) = Action.async {
    implicit request =>
    request.session.get("connected").map { connected =>
      userDao.getPermission(connected.toLong) match {
        case p if Constant.isManager(p) => {
          orderDao.findById(id).map(order => order match {
            case Some(o) => {
              val order = o.copy(assist = newAssist)
              orderDao.updateWait(id, order)
              val newOrder = orderDao.findByIdWait(id)
              Ok(Json.obj("id" -> id, "assist" -> newOrder.assist))
            }
            case None => Ok(Json.obj("error" -> "NotFound"))
          })
        }
        case p if !Constant.isManager(p) => {
          scala.concurrent.Future {}
          .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
    }
  }

  def UpdateShipping(
    id:Long,
    newShip:Int,
    newTrack:String
  ) = Action.async {
    implicit request =>
    request.session.get("connected").map { connected =>
      userDao.getPermission(connected.toLong) match {
        case p if Constant.isManager(p) => {
          orderDao.findById(id).map(order => order match {
            case Some(o) => {
              val order = o.copy(ship = Some(newShip), track = Some(newTrack))
              orderDao.updateWait(id, order)
              val newOrder = orderDao.findByIdWait(id)
              Ok(Json.obj("id" -> id, "ship" -> newOrder.ship, "track" -> newOrder.track))
            }
            case None => Ok(Json.obj("error" -> "NotFound"))
          })
        }
        case p if Constant.isUser(p) => {
          scala.concurrent.Future {}
          .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
    }
  }

  def UpdateStatus(id:Long, newStatus:Int) = Action.async {
    implicit request =>
    request.session.get("connected").map { connected =>
      userDao.getPermission(connected.toLong) match {
        case p if Constant.isManager(p) => {
          orderDao.findById(id).map(order => order match {
            case Some(o) => {
              val order = o.copy(status = newStatus)
              orderDao.updateWait(id, order)
              val newOrder = orderDao.findByIdWait(id)
              Ok(Json.obj("id" -> id, "status" -> newOrder.status))
            }
            case None => Ok(Json.obj("error" -> "NotFound"))
          })
        }
        case p if Constant.isUser(p) => {
          scala.concurrent.Future {}
          .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
        }
      }
    }.getOrElse {
      scala.concurrent.Future {}
      .map( _ =>  Ok(Json.obj("error" -> "NoPermission")) )
    }
  }
}
