# Acute: Kaixindeal.com order system

- Github page: https://github.com/janzhou/acute
- Support issues: https://github.com/janzhou/acute-support/issues

# No License

Copyright 2016 Jian Zhou. All Rights Reserved.
